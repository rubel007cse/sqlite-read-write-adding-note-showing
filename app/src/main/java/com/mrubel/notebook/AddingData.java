package com.mrubel.notebook;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AddingData extends AppCompatActivity {


    EditText e_title, e_details;
    RadioGroup rg;
    RadioButton r_low;
    Button sd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adding_data);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);

        e_title = (EditText) findViewById(R.id.mytitle);
        e_details = (EditText) findViewById(R.id.mydetails);

        rg = (RadioGroup) findViewById(R.id.myradiogroup);
        sd = (Button) findViewById(R.id.savedata);

        // creatinf database object

        final MyDbFunctions obj = new MyDbFunctions(this);



        sd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int selectedId=rg.getCheckedRadioButtonId();
                r_low = (RadioButton) findViewById(selectedId);

                int priority=0;
                if(r_low.equals("Low")){
                    priority = 1;
                } else if(r_low.equals("Medium")){
                    priority = 2;
                } else  if(r_low.equals("High")){
                    priority = 3;
                }


                // time
                Calendar cal = Calendar.getInstance();
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
                String time = sdf.format(cal.getTime());

                // titile details


                // inserting data
                obj.addNewNote(new TemplateForData(e_title.getText().toString(),
                        e_details.getText().toString(),
                        priority, time) );

                Toast.makeText(getApplicationContext(), "Note added successfully", Toast.LENGTH_LONG).show();


            }
        });


    }

}
