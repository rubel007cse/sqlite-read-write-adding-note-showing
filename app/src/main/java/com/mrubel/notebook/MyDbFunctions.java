package com.mrubel.notebook;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by RRF Admin on 5/13/2016.
 */
public class MyDbFunctions extends SQLiteOpenHelper {


    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_NAME = "NotebookDb";

    private static final String TABLE_NB = "nbtab2";

    private static final String KEY_ID = "id";
    private static final String KEY_TITLE = "title";
    private static final String KEY_DETAILS = "details";
    private static final String KEY_PRIORITY = "priority";
    private static final String KEY_TIME = "time";



    // CONSTRUCTOR
    public MyDbFunctions(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {


        String query = "CREATE TABLE " + TABLE_NB + "("
                + KEY_ID + " INTEGER PRIMARY KEY," +
                KEY_TITLE + " TEXT," + KEY_DETAILS + " TEXT," +
                KEY_PRIORITY + " INTEGER,"+
                KEY_TIME + " TEXT"+ ")";


        db.execSQL(query);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NB);


        onCreate(db);

    }








    // to insert data

    void addNewNote(TemplateForData td) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TITLE, td.get_title()); //&nbsp; topic
        values.put(KEY_DETAILS, td.get_details()); // details
        values.put(KEY_PRIORITY, td.get_priority());
        values.put(KEY_TIME, td.get_time());

        db.insert(TABLE_NB, null, values);
        db.close();

    }


     String[] getTtitle(){

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor res = db.rawQuery("select * from " + TABLE_NB + ";", null);
        res.moveToFirst();
        String[] p = new String[res.getCount()];

        if(res.moveToFirst()) {
            int i=0;
            do {
                p[i] =   res.getString(res.getColumnIndex("title"));
                i = i +1;
            } while(res.moveToNext());
        }
        return p;
    }

    String[] getDetails(){

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor res = db.rawQuery("select * from " + TABLE_NB + ";", null);
        res.moveToFirst();
        String[] p = new String[res.getCount()];

        if(res.moveToFirst()) {
            int i=0;
            do {
                p[i] =   res.getString(res.getColumnIndex("details"));
                i = i +1;
            } while(res.moveToNext());
        }
        return p;
    }

    String[] getTime(){

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor res = db.rawQuery("select * from " + TABLE_NB + ";", null);
        res.moveToFirst();
        String[] p = new String[res.getCount()];

        if(res.moveToFirst()) {
            int i=0;
            do {
                p[i] =   res.getString(res.getColumnIndex("time"));
                i = i +1;
            } while(res.moveToNext());
        }
        return p;
    }





    int[] getPriority(){

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor res = db.rawQuery("select * from " + TABLE_NB + ";", null);
        res.moveToFirst();
        int[] p = new int[res.getCount()];

        if(res.moveToFirst()) {
            int i=0;
            do {
                p[i] =   Integer.parseInt(res.getString(res.getColumnIndex("priority")));
                i = i +1;
            } while(res.moveToNext());
        }
        return p;
    }







}
