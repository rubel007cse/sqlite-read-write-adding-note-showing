package com.mrubel.notebook;

/**
 * Created by RRF Admin on 5/13/2016.
 */
public class TemplateForData {

    int _id;
    int _priority;
    String _title;
    String _details;
    String _time;

    // creating constructors

    public TemplateForData(){};

    public TemplateForData( String title, String details, int priority, String time){

        this._title = title;
        this._details = details;
        this._priority = priority;
        this._time = time;

    };
    public TemplateForData( int id, String title, String details, int priority, String time){

        this._id = id;
        this._title = title;
        this._details = details;
        this._priority = priority;
        this._time = time;

    };


    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }


    public String get_title() {
        return _title;
    }

    public void set_title(String _title) {
        this._title = _title;
    }


    public String get_details() {
        return _details;
    }

    public void set_details(String _details) {
        this._details = _details;
    }

    public int get_priority() {
        return _priority;
    }

    public void set_priority(int _priority) {
        this._priority = _priority;
    }


    public String get_time() {
        return _time;
    }

    public void set_time(String _time) {
        this._time = _time;
    }
}
