package com.mrubel.notebook;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    TextView tv;
    ListView lvps;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.WHITE);

        MyDbFunctions obj2 = new MyDbFunctions(this);

        String[] rec_title = obj2.getTtitle();
        String[] rec_details = obj2.getDetails();
        String[] rec_time = obj2.getTime();







            lvps = (ListView) findViewById(R.id.mylistview);


            CustomLVClass object = new CustomLVClass(MainActivity.this, rec_title, rec_details, rec_time );

            lvps.setAdapter(object);





        }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.add_new) {

            Intent i = new Intent(MainActivity.this, AddingData.class);
            startActivity(i);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
