package com.mrubel.notebook;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by RRF Admin on 4/29/2016.
 */
public class CustomLVClass extends BaseAdapter {
    String[] mytitle, mydetails, mytime;


    Context context;
    private static LayoutInflater inflater = null;

    TextView tv_tit;
    TextView tv_det;
    TextView tv_tim;

    public CustomLVClass(MainActivity mainActivity, String[] title,String[] details,String[] time) {
        // TODO Auto-generated constructor stub
        mytitle = title;
        mydetails = details;
        mytime = time;
        context = mainActivity;

        inflater = (LayoutInflater) context.
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mytitle.length;
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }



    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        View rowView;
        rowView = inflater.inflate(R.layout.customlvcdesign, null);


        tv_tit = (TextView) rowView.findViewById(R.id.cl_mytext);
        tv_tit.setText(mytitle[position]);

        tv_det = (TextView) rowView.findViewById(R.id.cl_details);
        tv_det.setText(mydetails[position]);

        tv_tim = (TextView) rowView.findViewById(R.id.cl_time);
        tv_tim.setText(mytime[position]);


        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Toast.makeText(context, "You Clicked " + mytitle[position], Toast.LENGTH_LONG).show();
            }
        });
        return rowView;
    }

}